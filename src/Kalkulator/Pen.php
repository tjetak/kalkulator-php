<?php

namespace Kalkulator;

class Pen extends Kalkulator
{
    /**
     * Get Pen specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('pen/spec');
    }

    /**
     * Get Pen price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('pen/price', $data);
    }
}
