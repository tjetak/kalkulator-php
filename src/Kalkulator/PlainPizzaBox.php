<?php

namespace Kalkulator;

class PlainPizzaBox extends Kalkulator
{
    /**
     * Get Plain Pizza Box specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-pizza-box/spec');
    }

    /**
     * Get Plain Pizza Box price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-pizza-box/price', $data);
    }
}
