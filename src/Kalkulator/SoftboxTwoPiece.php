<?php

namespace Kalkulator;


class SoftboxTwoPiece extends Kalkulator
{
    /**
     * Get Product Box - Two Piece specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('softbox-two-piece/spec');
    }

    /**
     * Get Product Box - Two Piece Price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('softbox-two-piece/price', $data);
    }
}
