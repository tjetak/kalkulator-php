<?php

namespace Kalkulator;

class TrayBox extends Kalkulator
{
    /**
     * Get Tray Box specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('tray-box/spec');
    }

    /**
     * Get Tray Box price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('tray-box/price', $data);
    }
}
