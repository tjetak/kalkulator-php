<?php

namespace Kalkulator;

class WallClock extends Kalkulator
{
    /**
     * Get wall clock specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('wall-clock/spec');
    }

    /**
     * Get wall clock price based on given specification
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('wall-clock/price', $data);
    }
}
