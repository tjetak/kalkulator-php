<?php

namespace Kalkulator;

class LidPlainPaperCupHot extends Kalkulator
{
    /**
     * Get Lid Plain Paper Cup Hot specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('lid-plain-paper-cup-hot/spec');
    }

    /**
     * Get Lid Plain Paper Cup Hot price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('lid-plain-paper-cup-hot/price', $data);
    }
}
