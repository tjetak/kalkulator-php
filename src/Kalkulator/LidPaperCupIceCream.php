<?php

namespace Kalkulator;

class LidPaperCupIceCream extends Kalkulator
{
    /**
     * Get Lid Paper Cup Ice Cream specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('lid-paper-cup-ice-cream/spec');
    }

    /**
     * Get Lid Paper Cup Ice Cream price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('lid-paper-cup-ice-cream/price', $data);
    }
}
