<?php

namespace Kalkulator;

class PlainCoffeeBag extends Kalkulator
{
    /**
     * Get Plain Coffee Bag specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-coffee-bag/spec');
    }

    /**
     * Get Plain Coffee Bag price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-coffee-bag/price', $data);
    }
}
