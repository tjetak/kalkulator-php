<?php


namespace Kalkulator;


class Card extends Kalkulator
{
    /**
     * Get Card specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('card/spec');
    }

    /**
     * Get Card price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('card/price', $data);
    }
}
