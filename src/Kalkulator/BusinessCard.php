<?php /** @noinspection ALL */

namespace Kalkulator;

class BusinessCard extends Kalkulator
{
    /**
     * Get Prices Data
     *
     * @param $data
     * @return mixed|string
     */
    public static function getPrices($data)
    {
        try {
            return self::post('business-card/price', $data);
        } catch (\Exception $e) {
            return "Caught Exception :" . $e->getMessage();
        }
    }

    /**
     * Get specifications data.
     *
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get("business-card/spec");
    }

    /**
     * Get Additional specification data
     *
     * @return mixed
     */
    public static function getAdditionalSpec(){
        return self::get("business-card/add-spec");
    }
}
