<?php


namespace Kalkulator;


class Poster extends Kalkulator
{
    /**
     * Get Poster specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('poster/spec');
    }

    /**
     * Get Poster price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('poster/price', $data);
    }
}
