<?php

namespace Kalkulator;

class PlainPouchBag extends Kalkulator
{
    /**
     * Get Plain Pouch Bag specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-pouch-bag/spec');
    }

    /**
     * Get Plain Pouch Bag price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-pouch-bag/price', $data);
    }
}
