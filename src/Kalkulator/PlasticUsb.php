<?php

namespace Kalkulator;

class PlasticUsb extends Kalkulator
{
    /**
     * Get Plastic Usb specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plastic-usb/spec');
    }

    /**
     * Get Plastic Usb Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plastic-usb/price', $data);
    }
}
