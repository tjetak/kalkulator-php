<?php

namespace Kalkulator;

class PlainPlasticBag extends Kalkulator
{
    /**
     * Get Plain Plastic Bag specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-plastic-bag/spec');
    }

    /**
     * Get Plain Plastic Bag price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-plastic-bag/price', $data);
    }
}
