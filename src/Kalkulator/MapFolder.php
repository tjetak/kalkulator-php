<?php

namespace Kalkulator;

class MapFolder extends Kalkulator
{
    public static function getPrices($data)
    {
        // TODO: Implement prices function
        return self::post("map-folder/price",$data);
    }

    /**
     * Get specifications.
     *
     * @return mixed
     */
    public static function spec()
    {
        return self::get("map-folder/spec");
    }
}
