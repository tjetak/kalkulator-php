<?php

namespace Kalkulator;

class SleeveCup extends Kalkulator
{
    /**
     * Get Sleeve Cup specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('sleeve-cup/spec');
    }

    /**
     * Get Sleeve Cup price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('sleeve-cup/price', $data);
    }
}
