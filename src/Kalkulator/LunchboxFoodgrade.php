<?php

namespace Kalkulator;


class LunchboxFoodgrade extends Kalkulator
{
    /**
     * Get Lunchbox Food Grade specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('lunchbox-foodgrade/spec');
    }

    /**
     * Get Lunchbox Food Grade price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('lunchbox-foodgrade/price', $data);
    }

     /**
     * Get Paper Cup Additional Specs
     * @return mixed
     */
    public static function getAdditionalSpecs()
    {
        return self::get('lunchbox-foodgrade/add-spec');
    }
}
