<?php

namespace Kalkulator;

class CeramicMug extends Kalkulator
{
    /**
     * Get Goodie Bag Press specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('ceramic-mug/spec');
    }

    /**
     * Get Goodie Bag Press Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('ceramic-mug/price', $data);
    }
}
