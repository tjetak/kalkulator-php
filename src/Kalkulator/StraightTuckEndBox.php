<?php

namespace Kalkulator;


class StraightTuckEndBox extends Kalkulator
{
    /**
     * Get Straight Tuck End Box specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('straight-tuck-end-box/spec');
    }

    /**
     * Get Straight Tuck End Box price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('straight-tuck-end-box/price', $data);
    }
}
