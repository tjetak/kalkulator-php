<?php

namespace Kalkulator;

class PaperCupIceCream extends Kalkulator
{
    /**
     * Get Plain Paper Cup Ice Cream specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('paper-cup-ice-cream/spec');
    }

    /**
     * Get Plain Paper Cup Ice Cream price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('paper-cup-ice-cream/price', $data);
    }
}
