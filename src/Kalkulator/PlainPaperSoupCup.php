<?php

namespace Kalkulator;

class PlainPaperSoupCup extends Kalkulator
{
    /**
     * Get Plain Paper Soup Cup specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-paper-soup-cup/spec');
    }

    /**
     * Get Plain Paper Soup Cup price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-paper-soup-cup/price', $data);
    }
}
