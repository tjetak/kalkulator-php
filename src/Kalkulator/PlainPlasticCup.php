<?php

namespace Kalkulator;

class PlainPlasticCup extends Kalkulator
{
    /**
     * Get Plain Plastic Cup specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-plastic-cup/spec');
    }

    /**
     * Get Plain Plastic Cup price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-plastic-cup/price', $data);
    }
}
