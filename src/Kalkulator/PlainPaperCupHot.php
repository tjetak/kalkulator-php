<?php

namespace Kalkulator;

class PlainPaperCupHot extends Kalkulator
{
    /**
     * Get Plain Paper Cup specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-paper-cup-hot/spec');
    }

    /**
     * Get Plain Paper Cup price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-paper-cup-hot/price', $data);
    }
}
