<?php

namespace Kalkulator;

class LidSealerPaperCup extends Kalkulator
{
    /**
     * Get Sealer specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('lid-sealer-paper-cup/spec');
    }

    /**
     * Get Sealer Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('lid-sealer-paper-cup/price', $data);
    }
}
