<?php

namespace Kalkulator;

class PlainLunchBox extends Kalkulator
{
    /**
     * Get Plain Lunch Box specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-lunch-box/spec');
    }

    /**
     * Get Plain Lunch Box price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-lunch-box/price', $data);
    }
}
