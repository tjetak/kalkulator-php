<?php


namespace Kalkulator;


class GreetingCard extends Kalkulator
{
    /**
     * Get Greeting Card specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('greeting-card/spec');
    }

    /**
     * Get Greeting Card price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('greeting-card/price', $data);
    }
}
