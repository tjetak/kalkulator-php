<?php

namespace Kalkulator;

class CardUsb extends Kalkulator
{
    /**
     * Get Card Usb specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('usb-card/spec');
    }

    /**
     * Get Card Usb Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('usb-card/price', $data);
    }
}
