<?php

namespace Kalkulator;


class SoftboxClassic extends Kalkulator
{
    /**
     * Get Product Box - Classic specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('softbox-classic/spec');
    }

    /**
     * Get Product Box - Classic Price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('softbox-classic/price', $data);
    }
}
