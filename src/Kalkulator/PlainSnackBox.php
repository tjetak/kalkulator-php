<?php

namespace Kalkulator;

class PlainSnackBox extends Kalkulator
{
    /**
     * Get Plain Paper Bag specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-snack-box/spec');
    }

    /**
     * Get Plain Paper Bag price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-snack-box/price', $data);
    }
}
