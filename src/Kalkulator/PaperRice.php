<?php

namespace Kalkulator;

class PaperRice extends Kalkulator
{
    /**
     * Get Paper Rice specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('paper-rice/spec');
    }

    /**
     * Get Paper Rice price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('paper-rice/price', $data);
    }
}
