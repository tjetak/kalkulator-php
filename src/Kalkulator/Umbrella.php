<?php


namespace Kalkulator;


class Umbrella extends Kalkulator
{
    /**
     * Get Umbrella specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('umbrella/spec');
    }

    /**
     * Get Umbrella price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('umbrella/price', $data);
    }
}
