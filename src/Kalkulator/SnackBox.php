<?php

namespace Kalkulator;


class SnackBox extends Kalkulator
{
    /**
     * Get Snack Box specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('snackbox/spec');
    }

    /**
     * Get Snack Box Price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('snackbox/price', $data);
    }
}