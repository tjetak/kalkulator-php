<?php

namespace Kalkulator;

class LidPlainPaperCupCold extends Kalkulator
{
    /**
     * Get Lid Plain Paper Cup Cold specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('lid-plain-paper-cup-cold/spec');
    }

    /**
     * Get Lid Plain Paper Cup Cold price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('lid-plain-paper-cup-cold/price', $data);
    }
}
