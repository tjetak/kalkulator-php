<?php

namespace Kalkulator;

class PlainBottle extends Kalkulator
{
    /**
     * Get Plain Plastic Bottle specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-bottle/spec');
    }

    /**
     * Get Plain Plastic Bottle price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-bottle/price', $data);
    }
}
