<?php

namespace Kalkulator;

class PlainPaperStraw extends Kalkulator
{
    /**
     * Get Plain Paper Straw specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-paper-straw/spec');
    }

    /**
     * Get Plain Paper Straw price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-paper-straw/price', $data);
    }
}
