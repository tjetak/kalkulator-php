<?php

namespace Kalkulator;


class IceCreamCup extends Kalkulator
{
    /**
     * Get Ice Cream Cup specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('ice-cream-cup/spec');
    }

    /**
     * Get Ice Cream Cup price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('ice-cream-cup/price', $data);
    }

     /**
     * Get Paper Cup Additional Specs
     * @return mixed
     */
    public static function getAdditionalSpecs()
    {
        return self::get('ice-cream-cup/add-spec');
    }
}
