<?php

namespace Kalkulator;


class PaperTray extends Kalkulator
{
    /**
     * Get Paper Tray specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('paper-tray/spec');
    }

    /**
     * Get Paper Tray price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('paper-tray/price', $data);
    }
}
