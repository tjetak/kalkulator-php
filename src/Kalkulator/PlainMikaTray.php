<?php

namespace Kalkulator;

class PlainMikaTray extends Kalkulator
{
    /**
     * Get Plain Mika Tray specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-mika-tray/spec');
    }

    /**
     * Get Plain Mika Tray price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-mika-tray/price', $data);
    }
}
