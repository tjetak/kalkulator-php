<?php


namespace Kalkulator;

class TumblerInsertPaper extends Kalkulator
{
    /**
     * Get insert paper tumbler specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('tumbler-insert-paper/spec');
    }

    /**
     * Get insert paper tumbler based on given specification
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('tumbler-insert-paper/price', $data);
    }
}
