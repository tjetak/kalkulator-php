<?php

namespace Kalkulator;


class Masker extends Kalkulator
{
    /**
     * Get Masker specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('masker/spec');
    }

    /**
     * Get Masker price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('masker/price', $data);
    }
}
