<?php

namespace Kalkulator;


class HardBoxTwoPiece extends Kalkulator
{
    /**
     * Get Hard Box - Two Piece specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('hardbox-two-piece/spec');
    }

    /**
     * Get Hard Box - Two Piece Price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('hardbox-two-piece/price', $data);
    }
}