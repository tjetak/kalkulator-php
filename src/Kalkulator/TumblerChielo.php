<?php

namespace Kalkulator;

class TumblerChielo extends Kalkulator
{
    /**
     * Get chielo tumbler specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('tumbler-chielo/spec');
    }

    /**
     * Get chielo tumbler based on given specification
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('tumbler-chielo/price', $data);
    }
}
