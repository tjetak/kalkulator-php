<?php

namespace Kalkulator;

class WoodenUsb extends Kalkulator
{
    /**
     * Get Wooden Usb specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('wooden-usb/spec');
    }

    /**
     * Get Wooden Usb Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('wooden-usb/price', $data);
    }
}
