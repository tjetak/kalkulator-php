<?php

namespace Kalkulator;

class PlasticCup extends Kalkulator
{
    /**
     *  Get plastic-cup's specification.
     *
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plastic-cup/spec');
    }

    /**
     * Get Plastic cup additional specification.
     *
     * @return mixed
     */
    public static function getAdditionalSpecs()
    {
        return self::get('plastic-cup/add-spec');
    }

    /**
     * Get Plastic Cup based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrices($data)
    {
        return self::post('plastic-cup/price', $data);
    }
}
