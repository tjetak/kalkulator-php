<?php

namespace Kalkulator;


class PastedLockBottomBox extends Kalkulator
{
    /**
     * Get Pasted Lock Bottom specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('pasted-lock-bottom-box/spec');
    }

    /**
     * Get Pasted Lock Bottom price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('pasted-lock-bottom-box/price', $data);
    }
}
