<?php

namespace Kalkulator;


class GolfHat extends Kalkulator
{
    /**
     * Get golf hat specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('golf-hat/spec');
    }

    /**
     * Get golf hat price based on given specifications
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('golf-hat/price', $data);
    }
}
