<?php

namespace Kalkulator;


class HardBoxCylinder extends Kalkulator
{
    /**
     * Get Hard Box - Cylinder specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('hardbox-cylinder/spec');
    }

    /**
     * Get Hard Box - Cylinder Price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('hardbox-cylinder/price', $data);
    }
}