<?php

namespace Kalkulator;

class MizzuStainlessTumbler extends Kalkulator
{
    /**
     * Get mizzu stainless tumbler specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('tumbler-mizzu-stainless/spec');
    }

    /**
     * Get mizzu stainless tumbler based on given specification
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('tumbler-mizzu-stainless/price', $data);
    }
}
