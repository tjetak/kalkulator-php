<?php

namespace Kalkulator;

class PaperBowl extends Kalkulator
{
    /**
     * Get paper-bowl specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('paper-bowl/spec');
    }

    /**
     * Get Paper Bowl Additional Specs
     * @return mixed
     */
    public static function getAdditionalSpecs()
    {
        return self::get('paper-bowl/add-spec');
    }

    /**
     * Get Paper Bowl Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('paper-bowl/price', $data);
    }
}
