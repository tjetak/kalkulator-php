<?php

namespace Kalkulator;


class MiniXBanner extends Kalkulator
{
    /**
     * Get Prices Data
     *
     * @param $data
     * @return mixed|string
     */
    public static function getPrices($data)
    {
        try {
            return self::post('mini-x-banner/price', $data);
        } catch (\Exception $e) {
            return "Caught Exception :" . $e->getMessage();
        }
    }

    /**
     * Get specifications data.
     *
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get("mini-x-banner/spec");
    }
}