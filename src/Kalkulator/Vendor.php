<?php

namespace Kalkulator;

use GuzzleHttp\Client;

class Vendor extends Kalkulator {

    public static function show($vendor_id)
    {
        return self::get('vendors/'.$vendor_id);
    }

    public static function index(){
        return self::get('vendors/');
    }

    public static function update($data) {
        return self::put('vendors/update', $data);
    }
}