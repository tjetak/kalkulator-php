<?php

namespace Kalkulator;

class PlainPaperCupCold extends Kalkulator
{
    /**
     * Get Plain Paper Cup Cold specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-paper-cup-cold/spec');
    }

    /**
     * Get Plain Paper Cup Cold price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-paper-cup-cold/price', $data);
    }
}
