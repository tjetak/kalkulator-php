<?php

namespace Kalkulator;

class YBanner extends Kalkulator
{
    /**
     * Get y-banner specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('y-banner/spec');
    }

    /**
     * Get y-banner price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('y-banner/price', $data);
    }
}
