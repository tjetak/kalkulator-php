<?php

namespace Kalkulator;

class LidPlasticCup extends Kalkulator
{
    /**
     * Get Lid Plastic Cup specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('lid-plastic-cup/spec');
    }

    /**
     * Get Lid Plastic Cup price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('lid-plastic-cup/price', $data);
    }
}
