<?php

namespace Kalkulator;

class PlainPaperBag extends Kalkulator
{
    /**
     * Get Plain Paper Bag specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-paper-bag/spec');
    }

    /**
     * Get Plain Paper Bag price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-paper-bag/price', $data);
    }
}
