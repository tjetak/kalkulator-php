<?php

namespace Kalkulator;

class BubbleStraw extends Kalkulator
{
    /**
     * Get Bubble Straw specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('bubble-straw/spec');
    }

    /**
     * Get Bubble Straw price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('bubble-straw/price', $data);
    }
}
