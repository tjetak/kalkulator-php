<?php

namespace Kalkulator;

class BentoBox extends Kalkulator
{
    /**
     * Get Bento Box specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('bento-box/spec');
    }

    /**
     * Get Bento Box price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('bento-box/price', $data);
    }
}
