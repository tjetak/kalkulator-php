<?php

namespace Kalkulator;

class Powerbank extends Kalkulator
{
    /**
     * Get Powerbank Press specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('powerbank/spec');
    }

    /**
     * Get Powerbank Press Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('powerbank/price', $data);
    }
}
