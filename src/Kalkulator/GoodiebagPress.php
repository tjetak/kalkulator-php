<?php

namespace Kalkulator;

class GoodiebagPress extends Kalkulator
{
    /**
     * Get Goodie Bag Press specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('goodiebag-press/spec');
    }

    /**
     * Get Goodie Bag Press Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('goodiebag-press/price', $data);
    }
}
