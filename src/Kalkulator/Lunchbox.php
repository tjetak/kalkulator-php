<?php

namespace Kalkulator;


class Lunchbox extends Kalkulator
{
    /**
     * Get Lunchbox specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('lunchbox/spec');
    }

    /**
     * Get Lunchbox Price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('lunchbox/price', $data);
    }
}