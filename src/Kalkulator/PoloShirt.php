<?php

namespace Kalkulator;


class PoloShirt extends Kalkulator
{

    /**
     * Get specifications data.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get("polo-shirt/spec");
    }

    /**
     * Get additional specifications data.
     * @return mixed
     */
    public static function getAdditionalSpec()
    {
        return self::get("polo-shirt/add-spec");
    }
    

    /**
     *
     * Get polo shirt Price based on it's data
     * @param $data
     * @return string
     */
    public static function getPrices($data)
    {
        try {
            return self::post('polo-shirt/price', $data);
        } catch (\Exception $e) {
            return "Caught Exception :" . $e->getMessage();
        }
    }
}