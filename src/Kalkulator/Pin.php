<?php

namespace Kalkulator;


class Pin extends Kalkulator
{
    /**
     * Get Corrugated Box specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('pin/spec');
    }

    /**
     * Get Corrugated box Price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('pin/price', $data);
    }

    /**
     * Get Additional specification data
     *
     * @return mixed
     */
    public static function getAdditionalSpec(){
        return self::get("pin/add-spec");
    }
}
