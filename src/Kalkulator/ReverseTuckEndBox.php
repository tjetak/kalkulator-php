<?php

namespace Kalkulator;


class ReverseTuckEndBox extends Kalkulator
{
    /**
     * Get Reverse Tuck End Box specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('reverse-tuck-end-box/spec');
    }

    /**
     * Get Reverse Tuck End Box price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('reverse-tuck-end-box/price', $data);
    }
}
