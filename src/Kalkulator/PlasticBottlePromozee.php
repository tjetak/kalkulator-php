<?php

namespace Kalkulator;


class PlasticBottlePromozee extends Kalkulator
{
    /**
     * Get Plastic Bottle Clio specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plastic-bottle-promozee/spec');
    }

    /**
     * Get Plastic Bottle Clio price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plastic-bottle-promozee/price', $data);
    }
}
