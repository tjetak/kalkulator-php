<?php

namespace Kalkulator;

class RubberUsb extends Kalkulator
{
    /**
     * Get Rubber Usb specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('rubber-usb/spec');
    }

    /**
     * Get Rubber Usb Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('rubber-usb/price', $data);
    }
}
