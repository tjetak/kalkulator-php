<?php

namespace Kalkulator;


class HardBoxTwoDoor extends Kalkulator
{
    /**
     * Get Hard Box - Two Door specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('hardbox-two-door/spec');
    }

    /**
     * Get Hard Box - Two Door Price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('hardbox-two-door/price', $data);
    }
}