<?php
/**
 * Created by PhpStorm.
 * User: farhad
 * Date: 21/01/19
 * Time: 19:01
 */

namespace Kalkulator;


class Bouquet extends Kalkulator
{
    /**
     * Get Prices Data
     *
     * @param $data
     * @return mixed|string
     */
    public static function getPrices($data)
    {
        try {
            return self::post('bouquet/price', $data);
        } catch (\Exception $e) {
            return "Caught Exception :" . $e->getMessage();
        }
    }

    /**
     * Get specifications data.
     *
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get("bouquet/spec");
    }
}