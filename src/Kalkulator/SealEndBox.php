<?php

namespace Kalkulator;


class SealEndBox extends Kalkulator
{
    /**
     * Get Seal End Box specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('seal-end-box/spec');
    }

    /**
     * Get Seal End Box price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('seal-end-box/price', $data);
    }
}
