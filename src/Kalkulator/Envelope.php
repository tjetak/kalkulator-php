<?php

namespace Kalkulator;


class Envelope extends Kalkulator
{
    /**
     * Get Envelope specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('envelope/spec');
    }

    /**
     * Get Envelope Price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('envelope/price', $data);
    }
}