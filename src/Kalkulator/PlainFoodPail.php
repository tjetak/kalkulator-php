<?php

namespace Kalkulator;


class PlainFoodPail extends Kalkulator
{
    /**
     * Get Plain Food Pail specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-food-pail/spec');
    }

    /**
     * Get Plain Food Pail price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-food-pail/price', $data);
    }
}
