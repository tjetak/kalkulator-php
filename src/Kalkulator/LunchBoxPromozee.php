<?php

namespace Kalkulator;


class LunchBoxPromozee extends Kalkulator
{
    /**
     * Get Food Container Clio specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('lunch-box-promozee/spec');
    }

    /**
     * Get Food Container Clio price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('lunch-box-promozee/price', $data);
    }
}
