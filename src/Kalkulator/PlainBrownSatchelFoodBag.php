<?php

namespace Kalkulator;


class PlainBrownSatchelFoodBag extends Kalkulator
{
    /**
     * Get Plain Brown Satchel Food Bag specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-brown-satchel-food-bag/spec');
    }

    /**
     * Get Plain Brown Satchel Food Bag price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-brown-satchel-food-bag/price', $data);
    }
}
