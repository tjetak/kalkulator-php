<?php

namespace Kalkulator;


class TuckTopAutoBottomBox extends Kalkulator
{
    /**
     * Get Tuck Top Auto Bottom specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('tuck-top-auto-bottom-box/spec');
    }

    /**
     * Get Tuck Top Auto Bottom price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('tuck-top-auto-bottom-box/price', $data);
    }
}
