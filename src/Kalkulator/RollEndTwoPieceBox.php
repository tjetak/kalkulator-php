<?php

namespace Kalkulator;


class RollEndTwoPieceBox extends Kalkulator
{
    /**
     * Get Roll End Two Piece specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('roll-end-two-piece-box/spec');
    }

    /**
     * Get Roll End Two Piece price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('roll-end-two-piece-box/price', $data);
    }
}
