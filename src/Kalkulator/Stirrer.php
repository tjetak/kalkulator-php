<?php

namespace Kalkulator;

class Stirrer extends Kalkulator
{
    /**
     * Get Stirrer specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('stirrer/spec');
    }

    /**
     * Get Stirrer price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('stirrer/price', $data);
    }
}
