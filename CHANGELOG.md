# Release Note
## v.0.1.51
### Added
- Masker
- Lunchbox promozee
- plastic bottole promozee
## v.0.1.51
### Added
- Plain coffe bag
- plain mika tray
- plain paper cup cold
- bento box
- bubble straw
- lid paper cup ice cream
- lid plain paper cup cold
- lid plain paper cup hot
- lid plastic cup
- paper cup ice cream paper rice
- plain paper soup cup
- plain paper straw
- plain pizza box
- plain plastic bag
- plain pouch bag
- sleeve cup
- stirrer
- tray box
## v.0.1.50
### Hotfix
- fix tumbler stainless and insert paper class
## v.0.1.49
### Hotfix
- paper paper cup hot class fix
- tumbler chielo class fix
## v.0.1.48
### Hotfix
- fix wrong class name plain bottle
## v.0.1.47
### Hotfix
- fix wrong class name plain bottle
## v.0.1.46
### Added
- add new plain snack box
### Hotfix
- fix class name for plain paper cup hot
## v.0.1.45
### Added
- add new plain lunchbox
- add new plain plastic cup
- add new plain paper cup
- add new plastic bottle
- add new plain paper bag
### Added
## v.0.1.44
### Added
- add new softbox classic
- add new softbox two piece
## v.0.1.43
### Added
- Cake Box Comb
- Food Pail Box
## v.0.1.42
### Added
- Lid Sealer Paper cup
- Lid sealer plastic cup
- plastic
## v.0.1.41
### Hotfix
- Pinched bottom paper bag price endpoint url
## v0.1.40
### Added
- square bottom paper bag
- pinched bottom paper bag
## v0.1.39
### Added
- bluetooth speaker
- mouse wireless
## v0.1.38
### Added
- Travel Adaptor
## v0.1.37
### Added
- Powerbank
## v0.1.36
### Added
- Ceramic Mug
## v0.1.35
### Added
- Tumbler
- Hat
- Goodie bag
- Garmen T Shirt
- Garmen Polo
## v0.1.34
- Promotional Products

## v0.1.33

## v.0.1.32
### Features
- Plain brown Food Box
- plain brown food paper bag
- plain brown satchel food bag
- plain food pail
- plain white food paper bag
- plain window brown food box
## v.0.1.31
### Features
- Burger box
- cake box
- Food Pail
- Lunchbox Food grade
- ice cream cup
- paper cup double wall
- Paper tray
- Wrapper
## v0.1.30
- plastic-bag-and-product-box-advance

## v0.1.29
- additional-spec-continuous-form

## v0.1.28 (2019-09-26)
### Added
- Corrugated Box
- Mailer Box

## v0.1.27 (2019-07-29)
### Added
- Kalkulator Bouquet

## v0.1.26 (2019-07-19)
### Hotfix
- fix class Continuous Form
## v0.1.25 (2019-07-19)
### Added
- Kalkulator Continuous Form
## v0.1.24 (2019-07-03)
### Added
- Kalkulator Calendar
## v0.1.23 (2019-07-02)
### Added
- Kalkulator Map Folder
## v0.1.22 (2019-05-21)
### Added
- Add class Get Products
## v0.1.21 (2019-05-09)
### Added
- Add class Umbrella
- Add class Pin
- Add class Corrugated

## v0.1.20 (2019-05-02)
### Hotfix
- fix endpoint greeting card get spec & get price

## v.0.1.19 (2019-05-02)
### Add
- Product Card
- Product Greeting card
- Product Poster

## v0.1.18 (2019-04-24)
## Add
- Product Fan
- Product Clock
## v0.1.16 (2019-03-16)
### Fix
- Fix SnackBox Class
### Add
- Product Box Classic Class
- Product Box Two Piece Class

## v0.1.15 (2019-03-05)
### Fix
- Fix All Class Hard Box URL
### Add
- Class Book

## v0.1.14 (2019-02-20)
### Add
- Class Lunchbox
- Class SnackBox
- Class PaperBag
- Class Hard Box Classic
- Class Hard Box Cylinder
- Class Hard Box Slide
- Class Hard Box Two Door
- Class Hard Box Two Piece

## v0.1.13 (2019-02-14)
### Add
- Class Sticker

## v0.1.12 (2019-02-07)
### Add
- Class Banner
- Class Envelope

## V0.1.11 (2019-01-29)
### Add
- Class Letterhead
- Class Mini X-Banner
- Class Tripod Banner

## v0.1.10 (2019-01-22)
### Add
- Class Note (NCR Note)
- Class Flyer
- Class Brochure

## v0.1.9 (2019-01-17)
### Add
- Class Spunbond

## v0.1.8 (2019-01-09)
### Add
- Class Polo Shirt
- Class T-Shirt
- Class Stamp

## v0.1.7 (2019-01-09)
### Fix
- change "form_params" to "json"

## v0.1.6 (2018-12-26)

### Add
- Add class Vendor
- Add class RollBanner

## v0.1.5 (2018-12-26)

### Add
- Add class Plastic Cup
- Add class Paper Bowl

### Fix
- Fix class Pop Up Counter (getPrices function)

## v0.1.4 (2018-12-21)

### Add
- Added class for x-banner
- Added class for y-banner

### Fix
- Fix BusinessCard class

## v0.1.3 (2018-12-21)

### Fix
- fix business card get price function
- fix Kalkulator post function

## v0.1.2 (2018-12-20)

### Added

- Added class for event-desk
- Added class for id-card
- Added class for pop-up-counter
- Added class for lanyard

### Updated

- Update class for business-card

## v0.1.1 (2018-12-19)

### Hotfix

- Fix PaperCup class, change how to use Kalkulator class static functions


## v0.1.0 (2018-12-19)

### Added

- Add PaperCup class with functions to get price, specifications, and additional specifications
- Add BusinessCard class with functions to get specifications
