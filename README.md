# Kalkulator PHP

Tjetak :heart: PHP!

This is the official Tjetak Kalkulator API wrapper in PHP.

Tjetak Kalkulator is an API which provides information on various printed & 
customized products price in Tjetak database. For more information about Tjetak 
Kalkulator, please visit https://calc.tjetak.com.

## Installation

Require this package with [Composer](https://getcomposer.org/) using the 
following command:

```shell
$ composer require tjetak/kalkulator-php
```

## How to Use

### Set the Key

Set the key with your Tjetak Kalkulator API key.

```php
Kalkulator::$key = "YOUR_API_KEY";
```

### Get Resources

Kalkulator PHP provides you the classes to get the corresponding resources. For
example, you can get the business cards specifications by using the following
code.

```php
use Kalkulator/BusinessCard;

function myFunction() {
    $specs = BusinessCard::specs(); // this returns business cards specifications
}
```

## Contributing

We welcome any contribution to the Kalkulator PHP! The contribution guide can be
found [here](CONTRIBUTING.md).

## License

Kalkulator PHP is open-sourced software licensed under the 
[MIT license](https://opensource.org/licenses/MIT).