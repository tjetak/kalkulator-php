<?php

namespace Unit;

use Kalkulator\Banner;
use Kalkulator\Kalkulator;
use PHPUnit\Framework\TestCase;

final class BannerTest extends TestCase
{
    var $businessCard;
    /**
     * Set up the test.
     */
    protected function setUp()
    {
        parent::setUp();

        Kalkulator::$key = "$2y$10$3nySAAD/4Oz9JFEqykx7NeY6CuUIOkjY44.mVcB67iBtAJnZhEfjy";
    }

    public function testHasSpecifications()
    {
        $data = Banner::getSpecs();
        $this->assertSame('success',$data->status);
        $this->assertObjectHasAttribute('data',$data);
    }

    public function testGetPrice(){
        $data = Banner::getPrices([
            'spec'=>[
                "material"=> "Flexy Korea China 385 GSM",
                "width"=> 3,
                "height"=> 3
            ],
            'loc'=>[
                "point"=> [
                    106.8224231,
                    -6.2112685
                ],
                "city"=> "Kota Jakarta Pusat"
            ],
            "quantities"=> [
                1,
                5,
                10,
                50,
                100
            ]
        ]);
        $this->assertSame('success',$data->status);
    }

    public function testWrongGetPrice(){
        $data = Banner::getPrices([
            'spec'=>[
                "material"=> "Flexy Korea China 300 GSM", // It's wrong value
                "width"=> 3,
                "height"=> 3
            ],
            'loc'=>[
                "point"=> [
                    106.8224231,
                    -6.2112685
                ],
                "city"=> "Kota Jakarta Pusat"
            ],
            "quantities"=> [
                1,
                5,
                10,
                50,
                100
            ]
        ]);
        $this->assertSame('error',$data->status);
    }

}
