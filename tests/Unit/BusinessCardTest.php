<?php

namespace Unit;

use Kalkulator\BusinessCard;
use Kalkulator\Kalkulator;
use PHPUnit\Framework\TestCase;

final class BusinessCardTest extends TestCase
{
    var $businessCard;
    /**
     * Set up the test.
     */
    protected function setUp()
    {
        parent::setUp();

        Kalkulator::$key = "api_key";
    }

//    public function testItCanGetPrice()
//    {
//        // TODO: Implement prices function test
//        $this->assertIsArray(BusinessCard::prices('Art Carton 260 GSM','2 Sisi',[1,10]));
//    }

    public function testHasPaperSpecification()
    {
        $this->assertObjectHasAttribute('paper',BusinessCard::specs());
    }

    public function testHasSideSpecification()
    {
        $this->assertObjectHasAttribute('sides',BusinessCard::specs());
    }
}
